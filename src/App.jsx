import './App.scss';
import Home from './pages/Home/Home.jsx';
import Favorites from './pages/Favorites/Favorites.jsx';
import Cart from './pages/Cart/Cart.jsx';
import { useState, useEffect } from 'react';
import { Route, Routes } from 'react-router-dom';

function App() {

  const [favorite, setFavorite] = useState([]);

  useEffect(() => {
    const itemsFavorite = JSON.parse(localStorage.getItem('favorite'));
    if (itemsFavorite) {
      setFavorite(itemsFavorite);
    }
  }, []);

  const toogleFavorite = ({e, id}) => {
    if(!favorite.includes(id)){
      e.target.classList.add('active');
      setFavorite((current) => {
        localStorage.setItem('favorite', JSON.stringify([...new Set([...current, id])]));
        return [...new Set([...current, id])];
      })
    } else {
      e.target.classList.remove('active');
      setFavorite((current) => {
        localStorage.setItem('favorite', JSON.stringify(current.filter((item) => item !== id)));
        return (current.filter((item) => item !== id))
      }
  );
    }
  }

  const [openModal, setOpenModal] = useState(false);
  const [id, setId] = useState(0);

  const openModalWin = ({id}) => {
      console.log("open", id);
      setId(id);
      document.body.classList.add('noscroll');
      setOpenModal(true);
  }

  const closeModalWin = () => {
      document.body.classList.remove('noscroll');
      setOpenModal(false);
  }

  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch('./storeData.json')
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setData(result);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, []);

  const [cart, setCart] = useState([]);


  useEffect(() => {
    const itemsCart = JSON.parse(localStorage.getItem('cart'));
    if (itemsCart) {
      setCart(itemsCart);
    }
  }, []);

  const addToCart = (id) => {
      setCart((prev) => {
        localStorage.setItem('cart', JSON.stringify([...new Set([...prev, id])]));
        return [...new Set([...prev, id])];
      })
  closeModalWin();
  }

  const deleteFromCart = (id) => {
    setCart((current) => {
      localStorage.setItem('cart', JSON.stringify(current.filter(item => item !== id)));
      return (current.filter((item) => item !== id));
    })
  closeModalWin();
  }


  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
      <Routes>
        <Route index element={<Home data={data} cart={cart} favorite={favorite}
        addToCart={addToCart} toogleFavorite={toogleFavorite} id={id}
        openModalWin={openModalWin} closeModalWin={closeModalWin} openModal={openModal}/>}/>
        <Route path="/favorites" element={<Favorites data={data} cart={cart} favorite={favorite} id={id}
        openModal={openModal} openModalWin={openModalWin} closeModalWin={closeModalWin} addToCart={addToCart}
        toogleFavorite={toogleFavorite}/>}/>
        <Route path="/cart" element={<Cart data={data} cart={cart} favorite={favorite} id={id}
        openModal={openModal} openModalWin={openModalWin} closeModalWin={closeModalWin} deleteFromCart={deleteFromCart}
        />}/>
      </Routes>
    );
  }
}

export default App;