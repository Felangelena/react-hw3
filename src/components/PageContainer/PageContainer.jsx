import './PageContainer.scss';
import Header from '../Header/Header.jsx';
import { useLocation } from 'react-router-dom';

const PageContainer = ({children, cart, favorite}) => {
    const {pathname} = useLocation();

    return (
        <div className="page-container">
            <Header cart={cart} favorite={favorite} pageName={pathname}/>
            {children}
        </div>
    );
}

export default PageContainer;