import PropTypes from "prop-types";
import './ProductListItem.scss';
import {ReactComponent as Star} from './icons/star.svg';
import Button from '../Button/Button.jsx';

const ProductListItem = ({id, title, color, price, image, openModalWin, toogleFavorite, hasBtnStar, hasBtnDel, btnTxt}) => {

    return (
        <li className="card">
            {hasBtnStar && <div className="card__favorite"><Star className="star" onClick={(e) => {toogleFavorite({id, e})}}/></div>}
            {hasBtnDel && <div className="card__delete" onClick={() =>{openModalWin({id})}}>X</div>}
            <img className="card__img" src={image} alt={title} />
            <h3 className="card__title">{title}</h3>
            <p className="card__color">Color: {color}</p>
            <p className="card__price">${price}</p>
            <Button className="card__btn" backgroundColor="black" text={btnTxt} handleClick={() => {openModalWin({id})}}/>
        </li>
    )
}

ProductListItem.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    openModalWin: PropTypes.func.isRequired,
}

ProductListItem.defaultProps = {
    color: "black",
    image: "image don't exist",
}

export default ProductListItem;