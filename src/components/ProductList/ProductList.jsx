import PropTypes from "prop-types";
import './ProductList.scss';
import ProductListItem from '../ProductListItem/ProductListItem.jsx';

const ProductList = ({data, openModalWin, toogleFavorite, hasBtnStar, hasBtnDel, btnTxt}) => {
    const listItems = data.map(item => (
        <ProductListItem key={item.id} id={item.id} title={item.title} color={item.color} price={item.price} image={item.image}
        openModalWin={openModalWin} toogleFavorite={toogleFavorite} hasBtnStar={hasBtnStar} hasBtnDel={hasBtnDel} btnTxt={btnTxt}
        />));

    return (
        <section className="arrivals box">
            <ul className="arrivals__grid">
                {listItems}
            </ul>
        </section>
    )
}

ProductList.propTypes = {
    data: PropTypes.array.isRequired,
    openModalWin: PropTypes.func.isRequired,
}

export default ProductList;