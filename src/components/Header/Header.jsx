import './Header.scss';
import {ReactComponent as Logo} from './icons/logo.svg';
import {ReactComponent as Favorites} from './icons/favorites.svg';
import {ReactComponent as Cart} from './icons/cart.svg';
import { Link } from 'react-router-dom';

const Header = ({cart, favorite, pageName}) => {
    return (
        <header className='header box'>
            <div className='header__box'>
                <Link to="/">
                    <Logo className='logo'/>
                </Link>
                <Link to="/favorites">
                    <Favorites className='favorites'/><span className='favorites-num'>{favorite.length}</span>
                </Link>
                <Link to="/cart">
                    <Cart className='cart'/><span className='cart-num'>{cart.length}</span>
                </Link>
            </div>
            <h1 className='header__title'>{pageName.substring(1)}</h1>
        </header>
    );
}

export default Header;