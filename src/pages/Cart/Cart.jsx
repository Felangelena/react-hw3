import './Cart.scss';
import PageContainer from '../../components/PageContainer/PageContainer.jsx';
import ProductList from '../../components/ProductList/ProductList.jsx';
import Button from '../../components/Button/Button.jsx';
import Modal from '../../components/Modal/Modal.jsx';

const Cart = ({data, cart, favorite, id, openModal, openModalWin, closeModalWin, deleteFromCart}) => {
    const selected = data.filter(({id}) => cart.includes(id));

    return (
        <PageContainer cart={cart} favorite={favorite}>
            <ProductList data={selected} openModalWin={openModalWin} hasBtnStar={false} hasBtnDel={true}
            btnTxt="delete"/>
            {openModal && <Modal header="Delete item?" isCloseButton={true} text="Do you want to delete this item from the cart?" close={closeModalWin}
            actions={[
                <Button key="1" className={'modal__btn'} backgroundColor="maroon" text="Ok" handleClick={() => {deleteFromCart(id)}}/>,
                <Button key="2" className={'modal__btn'} backgroundColor="maroon" text="Cancel" handleClick={closeModalWin}/>
            ]} />
            }
        </PageContainer>
    );
}

export default Cart;