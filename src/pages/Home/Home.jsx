import './Home.scss';
import PageContainer from '../../components/PageContainer/PageContainer.jsx';
import ProductList from '../../components/ProductList/ProductList.jsx';
import Button from '../../components/Button/Button.jsx';
import Modal from '../../components/Modal/Modal.jsx';

const Home = ({data, cart, favorite, openModalWin, closeModalWin, id, toogleFavorite, addToCart, openModal}) => {

    return (
        <PageContainer cart={cart} favorite={favorite}>
            <h2 className="arrivals__title">Latest arrivals</h2>
            <ProductList data={data} toogleFavorite={toogleFavorite}
            openModalWin={openModalWin} hasBtnStar={true} hasBtnDel={false} btnTxt="Add to cart"/>
            {openModal && <Modal header="Add item" isCloseButton={true} text="Do you want add item to the cart?" close={closeModalWin}
            actions={[
                <Button key="1" className={'modal__btn'} backgroundColor="maroon" text="Ok" handleClick={() => {addToCart(id)}}/>,
                <Button key="2" className={'modal__btn'} backgroundColor="maroon" text="Cancel" handleClick={closeModalWin}/>
            ]} />
            }
        </PageContainer>
    );
}

export default Home;